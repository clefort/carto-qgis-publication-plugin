Plugin QGIS (qgis-publication-plugin)

Ce plugin a été développé dans le cadre de la refonte du SIG de l'Agence de l'eau Loire-Bretagne (http://www.eau-loire-bretagne.fr) s'appuyant sur l'utilisation de logiciels libres et ayant pour ambition de faciliter et rationaliser l'acquisition, la valorisation, la diffusion et la présentation de l'information géographique. 
L'objectif de ce plugin est de faciliter la publication de projets cartographiques QGIS Desktop au sein de l'IDS et d'en conserver le plus fidèlement possible le langage cartographique. Ainsi, une carte et les couches qui la composent peuvent être poussées sur QGIS Server qui en propose la publication en Web service WMS. L'adresse de ce WMS est communiquée par courriel, il est alors possible de la référencer dans une application de Web Mapping ou en tant que WMS externe (WMS cascading) sur un autre serveur cartographique.
Le développement du plugin a été financé par l'Agence de l'eau Loire-Bretagne et réalisé par l'entreprise de services du numérique Capgemini (http://www.fr.capgemini.com/).

L'Agence de l'eau, établissement public français, s'engage depuis plus de 50 ans aux côtés des élus et des usagers de l'eau pour la qualité de l'eau et des milieux aquatiques.

L'utilisation de ce plug-in nécessite l'installation d'un webservice sur le serveur QGIS (carto-qgis-publication-ws https://bitbucket.org/clefort/carto-qgis-publication-ws).

L'URL du web service est à renseigner dans le fichier config.ini du plug-in.


Ce plug-in est diffusé sous licence GNU GPL 3 (https://www.gnu.org/licenses/gpl-3.0.fr.html), sans garantie et sans support utilisateur

Contact :
francois.lyonnais@eau-loire-bretagne.fr
