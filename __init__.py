# -*- coding: utf-8 -*-
"""
/***************************************************************************
 qgis-publication-plugin
                                 A QGIS plugin
 Ce plugin permet la publication de projets QGIS vers le serveur QGIS Server de Cart'O
                             -------------------
        begin                : 2016-10-19
        copyright            : (C) 2016 by Capgemini pour Agence de l’eau Loire-Bretagne
        email                : gregory.ferelloc@capgemini.com
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load PublishToQGISServer class from file PublishToQGISServer.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .publish_to_qgisserver import PublishToQGISServer
    return PublishToQGISServer(iface)
