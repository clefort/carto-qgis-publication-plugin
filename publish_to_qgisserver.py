# -*- coding: utf-8 -*-
"""
/***************************************************************************
 PublishToQGISServer
                                 A QGIS plugin
 Ce plugin permet la publication de projets QGIS vers le serveur QGIS Server de Cart'O
                              -------------------
        begin                : 2016-10-19
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Capgemini
        email                : gregory.ferelloc@capgemini.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
import json


from PyQt4.QtCore import QSettings, QTranslator, qVersion, QCoreApplication
from PyQt4.QtGui import QAction, QIcon
# Initialize Qt resources from file resources.py
from PyQt4.QtGui import QDialog, QMessageBox
from PyQt4.QtGui import QFrame
from PyQt4.QtGui import QGridLayout
from PyQt4.QtGui import QPushButton
from PyQt4.QtGui import QTextEdit

import resources
# Import the code for the dialog
from PublishToQGISServer.publish_to_qgisserver_dialog_base import Ui_PublishToQGISServerDialogBase
from connect_to_server import *
from publish_to_qgisserver_dialog import PublishToQGISServerDialog
import os.path
import ConfigParser
import getpass

from qgis.core import QgsMapLayerRegistry, QgsProject

class PublishToQGISServer:
    """QGIS Plugin Implementation."""

    wsurl = ""
    listUrl = ""
    listDB = ""
    timeout = None

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'PublishToQGISServer_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&Publish to QGISServer')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'PublishToQGISServer')
        self.toolbar.setObjectName(u'PublishToQGISServer')

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('PublishToQGISServer', message)

    def add_action(
            self,
            icon_path,
            text,
            callback,
            enabled_flag=True,
            add_to_menu=True,
            add_to_toolbar=True,
            status_tip=None,
            whats_this=None,
            parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        # Create the dialog (after translation) and keep reference
        self.dlg = PublishToQGISServerDialog()

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/PublishToQGISServer/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'Publication Cart\'O'),
            callback=self.run,
            parent=self.iface.mainWindow())

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&Publish to QGISServer'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar

    def crush(self):
        self.postRequest("True")
        QMessageBox.information(None, u"Publication Cart’O ",
                                u"Le transfert du projet est terminé. Une demande de publication a été adressée aux administrateurs fonctionnels de l’application.")
        self.close_dialogue()


    def close_dialogue(self):
        self.dlg.close()
        self.dlg.cancel.clicked.disconnect()
        self.dlg.ok.clicked.disconnect()

    def postRequest(self, overwrite="False"):
        username = getpass.getuser()
        print "current username = ", username
        url = wsurl + "/publishQGSIProject/upload"
        print "webservice url = ", url
        projectCompletePath = QgsProject.instance().fileName()
        print "project Path = ", projectCompletePath
        if projectCompletePath is None or projectCompletePath =="":
            print "Project not saved"
            QMessageBox.warning(None, u"Publication Cart’O ", u"Le projet n'est pas enregistré")
        else:
            files = {'file': open(projectCompletePath, 'rb'), 'username':username, "overwrite": overwrite}
            r = requests.post(url, files=files, timeout=timeout)
            print "Post request parameters: file = ", projectCompletePath, ";", "username = ", username, ";", "overwrite if file with same name exist = ", overwrite
            return r


    def publish(self):
        try:
            print "publishing ..."
            r = self.postRequest()
            if r.text == "Exist":
                print "a file with the same name exist"
                confirmOverwrite = QMessageBox()
                # confirmOverwrite.setIcon()
                confirmOverwrite.setWindowTitle(u"Publication Cart’O ")
                confirmOverwrite.setText(u"Vous avez déjà publié un projet de même nom sur le serveur. "
                                         u"En publiant ce projet, vous remplacerez le projet actuellement publié sur Cart’O.")
                confirmOverwrite.setInformativeText(u"Voulez-vous remplacer le projet existant ?")
                publishbutton = confirmOverwrite.addButton("Oui", QMessageBox.YesRole)
                quitButton = confirmOverwrite.addButton("Non", QMessageBox.NoRole)
                confirmOverwrite.setDefaultButton(quitButton)
                confirmOverwrite.exec_()
                if confirmOverwrite.clickedButton()== publishbutton:
                    self.postRequest("True")
                    QMessageBox.information(None, u"Publication Cart’O ",
                                            u"Le transfert du projet est terminé. Une demande de publication a été adressée aux administrateurs fonctionnels de l’application.")
                    self.close_dialogue()
            elif r.text == "Success":
                print "webservice response = ", r.text
                QMessageBox.information(None, u"Publication Cart’O ",
                                    u"Le transfert du projet est terminé. Une demande de publication a été adressée aux administrateurs fonctionnels de l’application.")
                self.close_dialogue()
        except:
            print "exception"
            QMessageBox.warning(None, u"Publication Cart’O ",
                                u"Le transfert du projet a echoué")

    def run(self):

        """Run method that performs all the real work"""

        # Get settings
        global wsurl
        global timeout

        config = ConfigParser.ConfigParser()
        config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'settings.ini'))
        wsurl = config.get('publicationWS', 'url')
        timeout = config.getfloat('publicationWS', 'timeout')
        print "webservice url= ", wsurl
        print "timeout= ", timeout

        listUrl = getURLList(wsurl, timeout)
        listDB = getHostList(wsurl, timeout)
        print "Authorized url = ", listUrl
        print "Authorizes Hosts = ", listDB
        if listUrl == "RequestException" or listDB == "RequestException":
            print "Exception: ", listUrl, listUrl
            QMessageBox.warning(None, u"Publication Cart’O ",
                                u"La connexion au service de publication a échouée.  "
                                u"Réessayez ultérieurement ou contactez l’administrateur de l’application.")
        else:
            # show the dialog
            self.dlg.show()
            self.dlg.textEdit.clear()
            self.dlg.ok.clicked.connect(lambda: self.publish())
            self.dlg.cancel.clicked.connect(lambda: self.close_dialogue())

            invalid_layer = {}
            layers = QgsMapLayerRegistry.instance().mapLayers().values()
            for layer in layers:
                if layer.providerType().lower() not in ['postgres', 'wms', 'wfs', 'wmts', 'tms']:
                    invalid_layer[layer.name()] = layer.providerType()
                else:
                    data_source = layer.dataProvider().dataSourceUri()
                    if layer.providerType() == 'postgres':
                        parameters = data_source.split(" ")
                        for i in parameters:
                            if i.find("host=") != -1:
                                host = i
                                break
                        hostformat = str(host.lstrip('host=')).lower()
                        if not listDB:
                            QMessageBox.warning(u"La liste des bases de données autorisées par le serveur est vide")
                        else:
                            if listDB.find(hostformat) == -1:
                                invalid_layer[layer.name()] = layer.providerType()
                    elif layer.providerType() == 'wms' or layer.providerType() == 'wfs':
                        parameters = data_source.split("&")
                        for i in parameters:
                            if i.find("url=") != -1:
                                wmsUrl = i
                                break
                        wmsUrlFormat1 = wmsUrl.lstrip('url=http://')
                        domain = wmsUrlFormat1.split("/")[0].lower()
                        if not listUrl:
                            QMessageBox.warning(u"La liste des webservices autorisés par le serveur est vide")
                        else:
                            if listUrl.find(domain) == -1:
                                invalid_layer[layer.name()] = layer.providerType()
            if invalid_layer:
                self.dlg.ok.setEnabled(False)
                self.dlg.textEdit.setText(
                    u"Le projet ne peut pas être publié en l’état. Pour publier le projet, retirez les couches suivantes :")
                for key in invalid_layer:
                    key_str = str(invalid_layer[key])
                    if key_str == 'postgres':
                        self.dlg.textEdit.append(key.upper() + " : couche " + repr(
                            key_str) + u" non accessible par le serveur ")
                    elif key_str == 'wms':
                        self.dlg.textEdit.append(key.upper() + " : couche " + repr(
                            key_str) + u" non accessible par le serveur ")
                    elif key_str == 'ogr' or key_str == 'gdal':
                        self.dlg.textEdit.append(key.upper() + " : " + " couche locale ")
                    else:
                        self.dlg.textEdit.append(key.upper() + " : couche " + repr(key_str))
            else:
                self.dlg.ok.setEnabled(True)
                self.dlg.textEdit.setText(u"Le projet est prêt pour publication. Cliquez sur le bouton « Publier le projet » "
                                          u"pour démarrer le transfert vers le serveur Cart’O. "
                                          u"\nCliquez sur le bouton « Quitter » pour annuler le transfert.")
