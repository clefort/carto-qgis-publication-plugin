# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'publish_to_qgisserver_dialog_base.ui'
#
# Created: Mon Nov 21 10:56:09 2016
#      by: PyQt4 UI code generator 4.10.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_PublishToQGISServerDialogBase(object):
    def setupUi(self, PublishToQGISServerDialogBase):
        PublishToQGISServerDialogBase.setObjectName(_fromUtf8("PublishToQGISServerDialogBase"))
        PublishToQGISServerDialogBase.setWindowModality(QtCore.Qt.NonModal)
        PublishToQGISServerDialogBase.resize(600, 500)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(PublishToQGISServerDialogBase.sizePolicy().hasHeightForWidth())
        PublishToQGISServerDialogBase.setSizePolicy(sizePolicy)
        PublishToQGISServerDialogBase.setMinimumSize(QtCore.QSize(600, 500))
        PublishToQGISServerDialogBase.setMaximumSize(QtCore.QSize(600, 500))
        PublishToQGISServerDialogBase.setModal(True)
        self.textEdit = QtGui.QTextEdit(PublishToQGISServerDialogBase)
        self.textEdit.setGeometry(QtCore.QRect(10, 20, 581, 411))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.textEdit.sizePolicy().hasHeightForWidth())
        self.textEdit.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.textEdit.setFont(font)
        self.textEdit.setAutoFillBackground(True)
        self.textEdit.setStyleSheet(_fromUtf8(""))
        self.textEdit.setFrameShape(QtGui.QFrame.NoFrame)
        self.textEdit.setAutoFormatting(QtGui.QTextEdit.AutoNone)
        self.textEdit.setTabChangesFocus(True)
        self.textEdit.setReadOnly(True)
        self.textEdit.setTabStopWidth(200)
        self.textEdit.setObjectName(_fromUtf8("textEdit"))
        self.cancel = QtGui.QPushButton(PublishToQGISServerDialogBase)
        self.cancel.setGeometry(QtCore.QRect(490, 460, 93, 28))
        self.cancel.setObjectName(_fromUtf8("cancel"))
        self.ok = QtGui.QPushButton(PublishToQGISServerDialogBase)
        self.ok.setGeometry(QtCore.QRect(372, 460, 111, 28))
        self.ok.setDefault(True)
        self.ok.setObjectName(_fromUtf8("ok"))

        self.retranslateUi(PublishToQGISServerDialogBase)
        QtCore.QMetaObject.connectSlotsByName(PublishToQGISServerDialogBase)

    def retranslateUi(self, PublishToQGISServerDialogBase):
        PublishToQGISServerDialogBase.setWindowTitle(_translate("PublishToQGISServerDialogBase", "Publication Cart'O", None))
        self.cancel.setText(_translate("PublishToQGISServerDialogBase", "Quitter ", None))
        self.ok.setText(_translate("PublishToQGISServerDialogBase", "Publier le projet", None))

