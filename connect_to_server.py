# -*- coding: utf-8 -*-

import requests
from PyQt4.QtGui import QMessageBox
from requests import RequestException


def getURLList(url, timeout):
    try:
        urlDomains = url + "/publishQGSIProject/authorizedDomains"
        print "urlDomains", urlDomains
        r = requests.get(urlDomains, timeout=timeout)
        listUrl = r.content.lower()
        return listUrl
    except RequestException:
        print "Exception: ", RequestException
        return "RequestException"


def getHostList(url, timeout):
    try:
        urlauthorizedHosts = url + "/publishQGSIProject/authorizedDatabase"
        r = requests.get(urlauthorizedHosts, timeout=timeout)
        listHosts = r.content.lower()
        return listHosts
    except RequestException:
        return "RequestException"
